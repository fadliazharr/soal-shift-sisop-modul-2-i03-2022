# soal-shift-sisop-modul-2-I03-2022



## Kelompok I-03
Erlangga Wahyu Utomo - 5025201118
Muhammad Fadli Azhar - 5025201157
Rahel Cecilia Purba  - 5025201155

## 2A
 Soal ini meminta kita untuk meng-unzip folder drakor.zip dan menghapus folder-folder yang tidak dibutuhkan. Namun praktikan hanya mampu meng-unzip folder drakor.zip . Praktikan tidak mampu menghapus folder-folder yang tidak perlu karena belum paham.
 • Pertama-tama kita membuat directory sebagai destinasi folder yang ingin di unzip

• Lalu kemudian kita fungsi zip extract menggunakan unzip

• Setelah itu kita membuat folder yang dimana sebagai tempatnya file file yang sudah di ekstrak.

• Kemudian masing-masing file dibuatkan folder untuk menyimpan sesuai dengan jenis file nya.


```
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void createdirectory (char *dest) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"mkdir", "-p", dest, NULL};
execv("/usr/bin/mkdir", argv);
} else {
((wait(&status)) > 0);
}
}

void zipextract(char *dest, char *src) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
execv("/usr/bin/unzip", argv);
} else {
((wait(&status)) > 0);
}
}

void copyfile(char *src, char *dest) {  
 int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"cp", src, dest, NULL};
execv("/usr/bin/cp", argv);
} else {
((wait(&status)) > 0);
}
}


int main() {

createdirectory ("/home/rahel/shift2/drakor/");
zipextract ("/home/rahel/shift2/drakor/", "/home/rahel/soal2/drakor.zip");
createdirectory ("/home/rahel/shift2/drakor/thriller");
createdirectory ("/home/rahel/shift2/drakor/horror");
createdirectory ("/home/rahel/shift2/drakor/action");
createdirectory ("/home/rahel/shift2/drakor/comedy");
createdirectory ("/home/rahel/shift2/drakor/romance");
createdirectory ("/home/rahel/shift2/drakor/school");
createdirectory ("/home/rahel/shift2/drakor/fantasy");

return 0;
}
```
![hasil2a](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-2-i03-2022/-/blob/main/imgs/1648388442996.png)

## 3A
Praktikan diminta untuk membuat 2 directory dengan nama darat dan 3 detik kemudian dengan nama air
![hasil31](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-2-i03-2022/-/blob/main/imgs/air.png)

## 3B
Program yang sudah dibuat harus dapat melakukan pengekstrakan folder animal.zip
![hasil32](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-2-i03-2022/-/blob/main/imgs/nomer_3_1.png)

## 3C
Hasil dari soal 3b harus dipisahkan berdasarkan jenis hewannya, hewan darat dan hewan air. rentang pembuatan folder air adalah 3 detik dimana folder darat lebih dulu. Untuk hewan yang tidak memiliki keterangan, harus dihapus
![hasil33](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-2-i03-2022/-/blob/main/imgs/nomer_3.png)
## 3D
 Praktikan diminta untuk menghapus semua burung yang ada di directory darat.
## 3e
  Praktikan harus membust file list.txt, dan membuat list nama semua hewan yang ada di directory air kedalam list.txt dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

## Kodingan untuk Nomor 3 
``` 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/wait.h>

void move (const char* path) {
	pid_t child_id, child, child2;
	int stat;
	DIR *dire;
	struct dirent *dr;
	char prnt[100001]; 
	char fol[10001];
	char txt[10001];
	if (!(dire = opendir(path))) {
	return;
	}
	while ((dr = readdir(dire)) != NULL) {
	if (strcmp(dr -> d_name, ".") == 0) {
	continue ;
	}
	child_id = fork();
	if (child_id == 0 ) {
	if (dr -> d_type == DT_DIR) {
		sprintf(prnt, "%s%s%s", "/home/erlangga2/modul2/animal/", dr->d_name, "/");
		sprintf(fol, "%s%s%s", "/home/erlangga2/modul2/darat/", dr->d_name, "/");
		sprintf(txt, "%s%s", prnt, "list.txt");
	child = fork();
	if (child == 0) {
	char *argv[] = {"cp", prnt, "/home/erlangga2/modul2/darat/", NULL};
	execv("/bin/cp", argv);
	} else {
	wait (NULL) ;
	child = fork();
	if (child2 == 0 ) {
	char *argv[] = {"touch", txt, "list.txt", NULL} ;
	execv("/usr/bin/touch", argv);
	}
	}
	} 
	else {
	sprintf(prnt, "%s%s", "/home/erlangga2/modul2/animal/", dr->d_name);
	char *argv[] = {"cp", prnt, "/home/erlangga2/modul2/air/", NULL};
	execv("/bin/cp", argv) ;
	}
}
}
	closedir(dire);
}
int main () {
	pid_t child1, child2, child3, child4;
	int stat;
	child1 = fork() ;
	if (child1 == 0) {
	child2 = fork();
	if (child2 == 0) {
	char *argv[] = {"mkdir", "/home/erlangga2/modul2/darat/", NULL};
	execv("/bin/mkdir", argv);
	} else {
	while ((wait(&stat)) >0) {
	sleep(3);
	char *argv[] = {"mkdir", "/home/erlangga2/modul2/air/", NULL};
	execv("/bin/mkdir", argv);
	}
	}
	}
	else {
	wait (NULL);
	child3 = fork ();
	if (child3 == 0 ) {
	child4 = fork();
	if(child4 == 0 ) {
	char *argv[] = {"unzip", "/home/erlangga2/modul2/animal.zip", "-d", "/home/erlangga2/modul2/", NULL};
	execv("/usr/bin/unzip", argv);
	}
	else {
	wait(NULL);
	move ("/home/erlangga2/modul2/animal/");
	}
	}
	else {
	wait(NULL);
	}
	}
}
```
