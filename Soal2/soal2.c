//2a

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void createdirectory (char *dest) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"mkdir", "-p", dest, NULL};
execv("/usr/bin/mkdir", argv);
} else {
((wait(&status)) > 0);
}
}

void zipextract(char *dest, char *src) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
execv("/usr/bin/unzip", argv);
} else {
((wait(&status)) > 0);
}
}

void copyfile(char *src, char *dest) {  
 int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"cp", src, dest, NULL};
execv("/usr/bin/cp", argv);
} else {
((wait(&status)) > 0);
}
}


int main() {

createdirectory ("/home/rahel/shift2/drakor/");
zipextract ("/home/rahel/shift2/drakor/", "/home/rahel/soal2/drakor.zip");
createdirectory ("/home/rahel/shift2/drakor/thriller");
createdirectory ("/home/rahel/shift2/drakor/horror");
createdirectory ("/home/rahel/shift2/drakor/action");
createdirectory ("/home/rahel/shift2/drakor/comedy");
createdirectory ("/home/rahel/shift2/drakor/romance");
createdirectory ("/home/rahel/shift2/drakor/school");
createdirectory ("/home/rahel/shift2/drakor/fantasy");

return 0;
}
